import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	
       float zuZahlenderBetrag; 
       float r�ckgabebetrag;
       int anzahl;

       
       anzahl = Fahrkartenautomat.fahrkartenbestellungErfassen();
       
       zuZahlenderBetrag = anzahl * 2.5f;
       
       // Geldeinwurf
       // -----------

       r�ckgabebetrag = Fahrkartenautomat.fahrkartenBezahlen(zuZahlenderBetrag);

       // Fahrscheinausgabe
       // -----------------
       
       Fahrkartenautomat.fahrkartenAusgeben();

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       
       Fahrkartenautomat.rueckgeldAusgeben(r�ckgabebetrag);
       
    }
    
    public static int fahrkartenbestellungErfassen () {
    	
    	Scanner tastatur = new Scanner(System.in);
    	int anzahl;
    	
    	System.out.println("Wie viele Fahrschein f�r je 2,50� m�chten Sie kaufen?");
        anzahl = tastatur.nextInt();
        
        return anzahl;
    }
    
    public static float fahrkartenBezahlen (float zuZahlenderBetrag) {
    	
    	Scanner tastatur = new Scanner(System.in);
    	float r�ckgeld;
        float eingezahlterGesamtbetrag;
        float eingeworfeneM�nze;
        
    	
    	eingezahlterGesamtbetrag = 0.0f;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f� \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneM�nze = tastatur.nextFloat();
            eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
        
    	
    	r�ckgeld = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    	
    	return r�ckgeld;
    }
    
    public static void fahrkartenAusgeben () {
    	
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
           } catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
           }
        }
        System.out.println("\n\n");
    	
    	return;
    }
    
    public static void rueckgeldAusgeben (float r�ckgabebetrag) {
    	
    	if(r�ckgabebetrag > 0.0)
        {
     	   System.out.print("Der R�ckgabebetrag in H�he von ");
     	   System.out.printf("%.2f EURO \n", r�ckgabebetrag);
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
         	  System.out.println("2 EURO");
 	          r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
         	  System.out.println("1 EURO");
 	          r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
         	  System.out.println("50 CENT");
 	          r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
         	  System.out.println("20 CENT");
  	          r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
         	  System.out.println("10 CENT");
 	          r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
         	  System.out.println("5 CENT");
  	          r�ckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.");
        
    	return;
    }
}